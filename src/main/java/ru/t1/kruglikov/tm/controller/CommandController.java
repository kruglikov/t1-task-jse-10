package ru.t1.kruglikov.tm.controller;

import ru.t1.kruglikov.tm.api.ICommandController;
import ru.t1.kruglikov.tm.api.ICommandRepository;
import ru.t1.kruglikov.tm.api.ICommandService;
import ru.t1.kruglikov.tm.model.Command;
import ru.t1.kruglikov.tm.util.NumberUtil;

public final class CommandController implements ICommandController {
    private final ICommandService commandService;

    private final ICommandRepository commandRepository;

    public CommandController(ICommandService commandService, ICommandRepository commandRepository){
        this.commandService = commandService;
        this.commandRepository = commandRepository;
    }

    @Override
    public void displayArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
    }

    @Override
    public void displayCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
    }

    @Override
    public void displayInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void displayArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void displayCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandRepository.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Kruglikov Andrey");
        System.out.println("akruglikov@t1-consulting.ru");
    }
}
