package ru.t1.kruglikov.tm.component;

import ru.t1.kruglikov.tm.api.*;
import ru.t1.kruglikov.tm.constant.ArgumentConst;
import ru.t1.kruglikov.tm.constant.CommandConst;
import ru.t1.kruglikov.tm.controller.CommandController;
import ru.t1.kruglikov.tm.controller.ProjectController;
import ru.t1.kruglikov.tm.controller.TaskController;
import ru.t1.kruglikov.tm.repository.CommandRepository;
import ru.t1.kruglikov.tm.repository.ProjectRepository;
import ru.t1.kruglikov.tm.repository.TaskRepository;
import ru.t1.kruglikov.tm.service.CommandService;
import ru.t1.kruglikov.tm.service.ProjectService;
import ru.t1.kruglikov.tm.service.TaskService;
import ru.t1.kruglikov.tm.util.TerminalUtil;

public final class Bootstrap {
    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService, commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENT:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMAND:
                commandController.displayCommands();
                break;
            default:
                commandController.displayArgumentError();
                break;
        }
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                commandController.displayAbout();
                break;
            case CommandConst.VERSION:
                commandController.displayVersion();
                break;
            case CommandConst.HELP:
                commandController.displayHelp();
                break;
            case CommandConst.INFO:
                commandController.displayInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            case CommandConst.ARGUMENT:
                commandController.displayArguments();
                break;
            case CommandConst.COMMAND:
                commandController.displayCommands();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTask();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            default:
                commandController.displayCommandError();
                break;
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void start(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");

        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");

            processCommand(TerminalUtil.nextLine());
        }
    }
}
