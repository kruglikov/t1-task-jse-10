package ru.t1.kruglikov.tm.service;

import ru.t1.kruglikov.tm.api.IProjectRepository;
import ru.t1.kruglikov.tm.api.IProjectService;
import ru.t1.kruglikov.tm.model.Project;
import java.util.List;

public final class ProjectService implements IProjectService {
    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name, final String description) {
        if (name==null || name.isEmpty()) return null;
        if (description==null) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public void add(Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }
}
