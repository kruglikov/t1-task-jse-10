package ru.t1.kruglikov.tm.api;

public interface IProjectController {
    void createProject();

    void showProjects();

    void clearProjects();
}
