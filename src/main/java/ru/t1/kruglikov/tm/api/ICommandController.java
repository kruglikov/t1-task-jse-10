package ru.t1.kruglikov.tm.api;

public interface ICommandController {
    void displayArgumentError();

    void displayCommandError();

    void displayInfo();

    void displayArguments();

    void displayCommands();

    void displayHelp();

    void displayVersion();

    void displayAbout();
}
