package ru.t1.kruglikov.tm.api;

import ru.t1.kruglikov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {
    void add(Task task);

    void clear();

    List<Task> findAll();
}
