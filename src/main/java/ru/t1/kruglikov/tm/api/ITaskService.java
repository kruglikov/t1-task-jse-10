package ru.t1.kruglikov.tm.api;

import ru.t1.kruglikov.tm.model.Task;

public interface ITaskService extends ITaskRepository {
    Task create(String name, String description);
}
