package ru.t1.kruglikov.tm.api;

public interface ITaskController {
    void createTask();

    void showTask();

    void clearTasks();
}
