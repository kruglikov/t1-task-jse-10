package ru.t1.kruglikov.tm.api;

import ru.t1.kruglikov.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
