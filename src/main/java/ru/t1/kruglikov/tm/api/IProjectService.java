package ru.t1.kruglikov.tm.api;

import ru.t1.kruglikov.tm.model.Project;

public interface IProjectService extends IProjectRepository {
    Project create(String name, String description);
}
