package ru.t1.kruglikov.tm.repository;

import ru.t1.kruglikov.tm.api.IProjectRepository;
import ru.t1.kruglikov.tm.model.Project;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }
}
